USE sql_work;
ALTER TABLE Projects ADD cost INT;

UPDATE Projects SET cost = 30000 WHERE proj_id = 1;
UPDATE Projects SET cost = 41000 WHERE proj_id = 2;
UPDATE Projects SET cost = 200000 WHERE proj_id = 3;
UPDATE Projects SET cost = 49000 WHERE proj_id = 4;
UPDATE Projects SET cost = 320000 WHERE proj_id = 5;
UPDATE Projects SET cost = 190000 WHERE proj_id = 6;
UPDATE Projects SET cost = 295000 WHERE proj_id = 7;
UPDATE Projects SET cost = 257000 WHERE proj_id = 8;
UPDATE Projects SET cost = 407000 WHERE proj_id = 9;
