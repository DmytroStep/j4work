USE sql_work;

-- Table4 Companies
INSERT INTO Companies (comp_id, comp_name) VALUES (1, 'Heracles');
INSERT INTO Companies (comp_id, comp_name) VALUES (2, 'MarsMacrosystems');
INSERT INTO Companies (comp_id, comp_name) VALUES (3, 'JBM');
INSERT INTO Companies (comp_id, comp_name) VALUES (4, 'MidiSoft');
INSERT INTO Companies (comp_id, comp_name) VALUES (5, 'APAM');

-- Table5 Customers
INSERT INTO Customers (cust_id, cust_name) VALUES (1, 'ABBII');
INSERT INTO Customers (cust_id, cust_name) VALUES (2, 'NewTech');
INSERT INTO Customers (cust_id, cust_name) VALUES (3, 'NASA');
INSERT INTO Customers (cust_id, cust_name) VALUES (4, 'PearInc');
INSERT INTO Customers (cust_id, cust_name) VALUES (5, 'Nintendo');

-- Table1 Developers
-- Company1
INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (1, 'Robert', 'Martin', 1, 5);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (2, 'Andrew', 'Stellmann', 1, 2);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (3, 'Ben', 'Forta', 1, 4);

-- Company2
INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (4, 'Joshua', 'Bloch', 2, 5);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (5, 'Rob', 'Harrop', 2, 5);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (6, 'Josh', 'Juneau', 2, 3);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (7, 'James', 'Gosling', 2, 7);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (25, 'David', 'Flanagan', 2, 7);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (8, 'Anil', 'Hemrajani', 2, 3);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (9, 'Paul', 'DuBois', 2, 6);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (10, 'David', 'Heffelfinger', 2, 5);

-- Company3
INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (11, 'Cay', 'Horstmann', 3, 6);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (12, 'Martin', 'Odersky', 3, 3);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (13, 'Guido', 'Van Rossum', 3, 3);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (14, 'Cris', 'Date', 3, 9);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (15, 'Paul', 'Deitel', 3, 6);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (16, 'Harvey', 'Deitel', 3, 4);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (17, 'Bruce', 'Eckel', 3, 5);


-- Company4
INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (18, 'Andrew', 'Troelsen', 4, 4);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (19, 'Brendan', 'Eich', 4, 3);

-- Company5
INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (20, 'Brian', 'Kernighan', 5, 9);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (21, 'Bierne', 'Stoustrope', 5, 8);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (22, 'Gary', 'Cornell', 5, 6);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (23, 'Jennifer', 'Greene', 5, 2);

INSERT INTO Developers (id, firstname, lastname, comp_id, experience)
VALUES (24, 'Stephen', 'Kochan', 5, 2);

-- Table2 Skills
INSERT INTO Skills (skill_id, skill) VALUES (1, 'C');
INSERT INTO Skills (skill_id, skill) VALUES (2, 'C++');
INSERT INTO Skills (skill_id, skill) VALUES (3, 'Java');
INSERT INTO Skills (skill_id, skill) VALUES (4, 'Python');
INSERT INTO Skills (skill_id, skill) VALUES (5, 'Scala');
INSERT INTO Skills (skill_id, skill) VALUES (6, 'Javascript');
INSERT INTO Skills (skill_id, skill) VALUES (7, 'C#');
INSERT INTO Skills (skill_id, skill) VALUES (8, 'DBA');
INSERT INTO Skills (skill_id, skill) VALUES (9, 'Objective-C');

-- Table6 Dev_Skills
-- Robert Martin
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (1, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (1, 3);

-- Andrew Stellmann
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (2, 7);

-- Ben Forta
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (3, 8);

-- Joshua Bloch
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (4, 3);

-- Rob Harrop
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (5, 3);

-- Josh Juneau
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (6, 3);

-- James Gosling
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (7, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (7, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (7, 3);

-- Anil Hemrajani
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (8, 3);

-- Paul DuBois
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (9, 8);

-- David Heffelfinger
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (10, 3);

-- Cay Horstmann
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (11, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (11, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (11, 3);

-- Martin Odersky
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (12, 3);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (12, 5);

-- Guido Van Rossum
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (13, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (13, 4);

-- Cris Date
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (14, 8);

-- Paul Deitel
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (15, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (15, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (15, 3);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (15, 7);

-- Harvey Deitel
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (16, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (16, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (16, 3);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (16, 7);

-- Bruce Eckel
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (17, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (17, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (17, 3);

-- Andrew Troelsen
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (18, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (18, 2);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (18, 7);

-- Brendan Eich
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (19, 6);

-- Brian Kernighan
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (20, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (20, 2);

-- Bierne Stroustrope
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (21, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (21, 2);

-- Gary Cornell
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (22, 1);
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (22, 3);

-- Jennifer Greene
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (23, 7);

-- Stephen Kochan
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (24, 9);

-- David Flanagan
INSERT INTO Dev_Skills (dev_id, skill_id) VALUES (25, 3);

-- Table7 Teams
-- 101
INSERT INTO Teams (team_id, dev_id) VALUES (101, 1);
INSERT INTO Teams (team_id, dev_id) VALUES (101, 2);
INSERT INTO Teams (team_id, dev_id) VALUES (101, 3);
-- 201
INSERT INTO Teams (team_id, dev_id) VALUES (201, 4);
INSERT INTO Teams (team_id, dev_id) VALUES (201, 5);
-- 202
INSERT INTO Teams (team_id, dev_id) VALUES (202, 6);
INSERT INTO Teams (team_id, dev_id) VALUES (202, 7);
INSERT INTO Teams (team_id, dev_id) VALUES (202, 25);
-- 203
INSERT INTO Teams (team_id, dev_id) VALUES (203, 8);
INSERT INTO Teams (team_id, dev_id) VALUES (203, 9);
INSERT INTO Teams (team_id, dev_id) VALUES (203, 10);
-- 301
INSERT INTO Teams (team_id, dev_id) VALUES (301, 11);
INSERT INTO Teams (team_id, dev_id) VALUES (301, 12);
INSERT INTO Teams (team_id, dev_id) VALUES (301, 13);
-- 302
INSERT INTO Teams (team_id, dev_id) VALUES (302, 14);
INSERT INTO Teams (team_id, dev_id) VALUES (302, 15);
INSERT INTO Teams (team_id, dev_id) VALUES (302, 16);
INSERT INTO Teams (team_id, dev_id) VALUES (302, 17);
-- 401
INSERT INTO Teams (team_id, dev_id) VALUES (401, 18);
INSERT INTO Teams (team_id, dev_id) VALUES (401, 19);
-- 501
INSERT INTO Teams (team_id, dev_id) VALUES (501, 20);
INSERT INTO Teams (team_id, dev_id) VALUES (501, 21);
-- 502
INSERT INTO Teams (team_id, dev_id) VALUES (502, 22);
INSERT INTO Teams (team_id, dev_id) VALUES (502, 23);
INSERT INTO Teams (team_id, dev_id) VALUES (502, 24);


-- Table3 Projects
INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (1, 'HelloWorld', 5, 501, 2);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (2, 'SpringTest', 2, 201, 4);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (3, 'OrbitSim', 3, 301, 3);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (4, 'SuperMario', 4, 401, 5);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (5, 'LingEngine', 3, 302, 1);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (6, 'ATMPlus', 1, 101, 2);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (7, 'ZTranslator', 2, 202, 1);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (8, 'PhoneSupport', 5, 502, 4);

INSERT INTO Projects (proj_id, proj_name, comp_id, team_id, cust_id)
VALUES (9, 'MultiDB', 2, 203, 2);

