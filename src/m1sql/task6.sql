-- ���������� ������� �������� ������������� � �������,
-- ������� �������� ���������� �������

USE sql_work;
SELECT 'Average salary of minCost project' FROM Dual;
SELECT AVG(salary) AS Average_salary FROM Developers
WHERE id IN (
	SELECT dev_id FROM Teams
	WHERE team_id IN (
		SELECT team_id FROM Projects
		WHERE cost = (
			SELECT MIN(cost) FROM Projects
		)
	)
);
