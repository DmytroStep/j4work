DROP DATABASE IF EXISTS sql_work;
CREATE DATABASE sql_work;
USE sql_work;

-- 1 Developers
CREATE TABLE Developers(
	id INT 				    NOT NULL,
	firstname VARCHAR(20) NOT NULL,
	lastname  VARCHAR(30) NOT NULL,
	comp_id INT 		    NOT NULL,
	experience INT		    NOT NULL
);

-- 2 Skills
CREATE TABLE Skills(
	skill_id INT 		 NOT NULL,
	skill VARCHAR(20)  NOT NULL
);

-- 6 Dev_Skills
CREATE TABLE Dev_Skills(
	dev_id   INT NOT NULL,
	skill_id INT NOT NULL
);

-- 3 Projects
CREATE TABLE Projects(
	proj_id INT 			 NOT NULL,
	proj_name VARCHAR(50) NOT NULL,
	comp_id INT 			 NOT NULL,
	team_id INT 			 NOT NULL,
	cust_id INT 			 NOT NULL
);

-- 4 Companies
CREATE TABLE Companies(
	comp_id INT 			 NOT NULL,
	comp_name VARCHAR(50) NOT NULL
);

-- 5 Customers
CREATE TABLE Customers(
	cust_id INT 			 NOT NULL,
	cust_name VARCHAR(50) NOT NULL
);

-- 7 Teams
CREATE TABLE Teams(
	team_id INT	 NOT NULL,
	dev_id  INT  NOT NULL
);

--
ALTER TABLE Developers ADD PRIMARY KEY (id);
ALTER TABLE Skills ADD PRIMARY KEY (skill_id);
ALTER TABLE Projects ADD PRIMARY KEY (proj_id);
ALTER TABLE Companies ADD PRIMARY KEY (comp_id);
ALTER TABLE Customers ADD PRIMARY KEY (cust_id);

--
ALTER TABLE Developers ADD CONSTRAINT FK_Devs_Comps  FOREIGN KEY (comp_id)   REFERENCES Companies (comp_id);
ALTER TABLE Dev_Skills ADD CONSTRAINT FK_DS_Devs     FOREIGN KEY (dev_id)    REFERENCES Developers (id);
ALTER TABLE Dev_Skills ADD CONSTRAINT FK_DS_Skills   FOREIGN KEY (skill_id)  REFERENCES Skills (skill_id);
ALTER TABLE Projects ADD CONSTRAINT FK_Projs_Comps FOREIGN KEY (comp_id)   REFERENCES Companies (comp_id);
ALTER TABLE Projects ADD CONSTRAINT FK_Projs_Custs FOREIGN KEY (cust_id)   REFERENCES Customers (cust_id);
ALTER TABLE Teams ADD CONSTRAINT FK_Teams_Devs  FOREIGN KEY (dev_id)    REFERENCES Developers (id);
