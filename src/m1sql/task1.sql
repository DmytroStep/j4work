USE sql_work;
ALTER TABLE Developers ADD salary INT;

UPDATE Developers SET salary = 4850 WHERE id = 1;
UPDATE Developers SET salary = 2500 WHERE id = 2;
UPDATE Developers SET salary = 4100 WHERE id = 3;

UPDATE Developers SET salary = 4900 WHERE id = 4;
UPDATE Developers SET salary = 4930 WHERE id = 5;
UPDATE Developers SET salary = 2900 WHERE id = 6;
UPDATE Developers SET salary = 5500 WHERE id = 7;
UPDATE Developers    SET salary = 5450 WHERE id = 25;
UPDATE Developers SET salary = 3000 WHERE id = 8;
UPDATE Developers SET salary = 5000 WHERE id = 9;
UPDATE Developers SET salary = 4700 WHERE id = 10;

UPDATE Developers SET salary = 5100 WHERE id = 11;
UPDATE Developers SET salary = 2900 WHERE id = 12;
UPDATE Developers SET salary = 2870 WHERE id = 13;
UPDATE Developers SET salary = 7100 WHERE id = 14;
UPDATE Developers SET salary = 5500 WHERE id = 15;
UPDATE Developers SET salary = 4400 WHERE id = 16;
UPDATE Developers SET salary = 5000 WHERE id = 17;

UPDATE Developers SET salary = 4100 WHERE id = 18;
UPDATE Developers SET salary = 3000 WHERE id = 19;

UPDATE Developers SET salary = 7000 WHERE id = 20;
UPDATE Developers SET salary = 6500 WHERE id = 21;
UPDATE Developers SET salary = 5300 WHERE id = 22;
UPDATE Developers SET salary = 2500 WHERE id = 23;
UPDATE Developers SET salary = 2650 WHERE id = 24;
