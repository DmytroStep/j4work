-- ����������� ������ �������� ������� (������ �� �������� �������������)

USE sql_work;
DROP VIEW IF EXISTS ProjectSalaries;

-- ���������� ������
CREATE VIEW ProjectSalaries AS 
SELECT proj_id, proj_name, SUM(salary) AS proj_salary FROM Developers, Teams, Projects
WHERE Developers.id = Teams.dev_id AND
		Teams.team_id = Projects.team_id AND
		Teams.team_id IN (
			SELECT team_id FROM Projects
		)
GROUP BY Teams.team_id;

-- ����������� ������ �������� �������
SELECT proj_id, proj_name, proj_salary FROM ProjectSalaries
WHERE proj_salary IN (
	SELECT MAX(proj_salary) FROM ProjectSalaries
);

